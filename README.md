# Whitaker Courts App

App for Android to display the status of the University of Texas at Austin Whitaker Tennis Courts Complex. 

This app is compliant with the [utrecsports robot exclusion policy](http://www.utrecsports.org/robots.txt), and is meant to be nothing more than a free app to access utrecsports data easily from an Android device.

This app uses [JSoup](http://jsoup.org/) to scrape data off of the [UTexas Rec Sports page](http://www.utrecsports.org/hours), and present the user with a simple status of the Whitaker Tennis Courts Complex.

Currently there is only one fragment (SimpleInfoFragment) which displays the user with the status of the courts (OPEN/CLOSED/IDK MY BFF JILL), and the times that the courts are open for the current day.

The network call receives all of the court's times for the entire week, and will make sure not to make another network call until the following week. It does this by storing the data in [SharedPreferences](http://developer.android.com/guide/topics/data/data-storage.html) and then making sure to check if they need to be updated whenever the app starts up.

![OPEN_COURTS](http://i.imgur.com/Rw2jnBe.png)
