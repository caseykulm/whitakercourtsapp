package com.mlukyesac.whitakercourts;

import com.mlukyesac.whitakercourts.fragments.SimpleInfoFragment;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

/**
 * @author mlukyesac
 *
 * Whitaker Courts App - Screen scrapes information about tennis courts hours 
 * and displays their status. Data for the day is persisted so that it never makes 
 * more than one network call per day using essentially no data, and the amount of data 
 * persisted is essentially nothing giving an extremely lightweight app.
 */
public class MainActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		startFragments();
	}

	
	/**
	 * Kicks off the fragment management, defaulting to the SimpleInfoFragment to start.
	 */
	private void startFragments() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

		SimpleInfoFragment simpleInfoFrag = new SimpleInfoFragment();
		fragmentTransaction.add(R.id.main_activity, simpleInfoFrag);
		fragmentTransaction.commit();
	}

}
