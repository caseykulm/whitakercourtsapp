package com.mlukyesac.whitakercourts.listeners;

import com.mlukyesac.whitakercourts.models.CourtDayTimes;

public interface WhitakerCourtsListener {	

	public void thisWeekTimesChanged(CourtDayTimes thisWeek, CourtDayTimes thisSaturday, CourtDayTimes thisSunday);
}
