package com.mlukyesac.whitakercourts.fragments;

import java.util.Calendar;
import java.util.TimeZone;

import com.mlukyesac.whitakercourts.R;
import com.mlukyesac.whitakercourts.controllers.FetchThisWeekTimes;
import com.mlukyesac.whitakercourts.listeners.WhitakerCourtsListener;
import com.mlukyesac.whitakercourts.models.CourtDayTimes;
import com.mlukyesac.whitakercourts.models.WhitakerCourts;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class SimpleInfoFragment extends Fragment implements WhitakerCourtsListener{

	private static String TAG = "SimpleInfoFragment";
	public static final String PREFS_NAME = "MyPrefsFile";
	private Context mainContext;
	private TextView courtsStatusTextView;
	private TextView courtsTimesTextView;
	private View simpleFragView;
	private WhitakerCourts courts;
	private String mRawWeekStr, mRawSaturdayStr, mRawSundayStr;
	private int mThisWeek, mThisYear;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.simple_info_fragment, container, false);

		Calendar calendar = Calendar.getInstance();
		mThisWeek = calendar.get(Calendar.WEEK_OF_YEAR);
		mThisYear = calendar.get(Calendar.YEAR);
		
		simpleFragView = view;
		mainContext = container.getContext();
		courtsStatusTextView = (TextView) view.findViewById(R.id.courts_status);
		courtsStatusTextView.setVisibility(View.INVISIBLE);
		courtsTimesTextView = (TextView) view.findViewById(R.id.courts_times);
		courtsTimesTextView.setVisibility(View.INVISIBLE);

		courts = new WhitakerCourts();

		loadPersistedData();
		checkPersistedData();

		return view;
	}

	private void checkPersistedData() {

		if(!courts.updatedThisWeek(mThisWeek, mThisYear) || !allDataPersisted()) {
            FetchThisWeekTimes thisWeekTimesTask = new FetchThisWeekTimes((Activity) this.mainContext, this);
			thisWeekTimesTask.execute();
		}
		else {
			courts.setThisWeekWeekdayHours(new CourtDayTimes(this.mRawWeekStr));
			courts.setThisSaturdayHours(new CourtDayTimes(this.mRawSaturdayStr));
			courts.setThisSundayHours(new CourtDayTimes(this.mRawSundayStr));

			updateStatus();
			updateTimesTextView();
		}
		
	}

    private boolean allDataPersisted() {
        if(this.mRawSundayStr.equals("")) {
            Log.i(TAG, "This Sunday is not persisted.");
            return false;
        }
        else if(this.mRawWeekStr.equals("")) {
            Log.i(TAG, "This week's weekdays are not persisted.");
            return false;
        }
        else if(this.mRawSaturdayStr.equals("")) {
            Log.i(TAG, "This Saturday is not persisted.");
            return false;
        }
        else {
            Log.i(TAG, "All data for this week is persisted.");
            return true;
        }
    }

	private void loadPersistedData() {
		// Restore preferences
		SharedPreferences settings = mainContext.getSharedPreferences(PREFS_NAME, 0);
		this.mRawWeekStr = settings.getString("rawWeekStr", "");
		this.mRawSaturdayStr = settings.getString("rawSaturdayStr", "");
		this.mRawSundayStr = settings.getString("rawSundayStr", "");
		
		courts.setLastWeekUpdated(settings.getInt("lastWeekUpdated", -1));
		courts.setLastYearUpdated(settings.getInt("lastYearUpdated", -1));
	}

	public void updateStatus() {
		boolean courtsOpenNow = courts.openNow();

		if(courtsOpenNow)
			updateStatus(WhitakerCourts.CURR_STATUS.OPEN);
		else
			updateStatus(WhitakerCourts.CURR_STATUS.CLOSED);
	}

	public void updateStatus(WhitakerCourts.CURR_STATUS status) {

		if(status == WhitakerCourts.CURR_STATUS.CLOSED) {
			courtsStatusTextView.setText(R.string.status_text_closed);
			courtsStatusTextView.setTextColor(simpleFragView.getResources().getColor(R.color.status_text_color_closed));
		}
		else if(status == WhitakerCourts.CURR_STATUS.UNKNOWN) {
			courtsStatusTextView.setText(R.string.status_text_unknown);
			courtsStatusTextView.setTextColor(simpleFragView.getResources().getColor(R.color.status_text_color_unknown));
		}
		else if(status == WhitakerCourts.CURR_STATUS.OPEN) {
			courtsStatusTextView.setText(R.string.status_text_open);
			courtsStatusTextView.setTextColor(simpleFragView.getResources().getColor(R.color.status_text_color_open));
		}
		else {
			Log.e(TAG, "status of the courts is really weird");
		}

		courtsStatusTextView.setVisibility(View.VISIBLE);
	}

	@Override
	public void thisWeekTimesChanged(CourtDayTimes thisWeek, CourtDayTimes thisSaturday, CourtDayTimes thisSunday) {

		courts.setThisWeekWeekdayHours(thisWeek);
		courts.setThisSaturdayHours(thisSaturday);
		courts.setThisSundayHours(thisSunday);

		mRawWeekStr = courts.getThisWeekWeekdayHours().getRawCourtTimes();
		mRawSaturdayStr = courts.getThisSaturdayHours().getRawCourtTimes();
		mRawSundayStr = courts.getThisSundayHours().getRawCourtTimes();

		updateTimesTextView();
		updateStatus();
	}

	private void updateTimesTextView() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone("America/Chicago"));
		int currDay = calendar.get(Calendar.DAY_OF_WEEK);

		String todayT = "Today's times: ";

		switch (currDay) {
		case Calendar.SATURDAY:
			CourtDayTimes saturdayTimes = courts.getThisSaturdayHours();
			courtsTimesTextView.setText(todayT+saturdayTimes.getRawCourtTimes());
			break;
		case Calendar.SUNDAY:
			CourtDayTimes sundayTimes = courts.getThisSundayHours();
			courtsTimesTextView.setText(todayT+sundayTimes.getRawCourtTimes());
			break;
		default:
			CourtDayTimes weekdayTimes = courts.getThisWeekWeekdayHours();
			courtsTimesTextView.setText(todayT+weekdayTimes.getRawCourtTimes());
		}
		
		courtsTimesTextView.setVisibility(View.VISIBLE);
	}

	@Override
	public void onStop() {
		super.onStop();
		persistData();
	}

	private void persistData() {
		// We need an Editor object to make preference changes.
		// All objects are from android.context.Context
		SharedPreferences settings = mainContext.getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("rawWeekStr", this.mRawWeekStr);
		editor.putString("rawSaturdayStr", this.mRawSaturdayStr);
		editor.putString("rawSundayStr", this.mRawSundayStr);
		editor.putInt("lastWeekUpdated", this.courts.getLastWeekUpdated());
		editor.putInt("lastYearUpdated", this.courts.getLastYearUpdated());
		
		// Commit the edits
		editor.commit();
	}

}
