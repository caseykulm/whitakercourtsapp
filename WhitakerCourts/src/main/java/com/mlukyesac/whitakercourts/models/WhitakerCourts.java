package com.mlukyesac.whitakercourts.models;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import com.mlukyesac.whitakercourts.models.CourtDayTimes.TimeFrame;

public class WhitakerCourts {

	private CourtDayTimes thisWeekWeekdayHours, thisSaturdayHours, thisSundayHours;
	private int lastWeekUpdated, lastYearUpdated;

    public enum CURR_STATUS {
        OPEN, CLOSED, UNKNOWN
    }

	public WhitakerCourts() {
		
	}
	
	public WhitakerCourts(int lastWeekUpdated, int lastYearUpdated) {
		this.lastWeekUpdated = lastWeekUpdated;
		this.lastYearUpdated = lastYearUpdated;
	}

	public boolean updatedThisWeek(int currentWeek, int currentYear) {
		boolean updatedThisWeek = (lastWeekUpdated == currentWeek) && (lastYearUpdated == currentYear);
		
		if(!updatedThisWeek) {
			this.lastWeekUpdated = currentWeek;
			this.lastYearUpdated = currentYear;
		}
		
		return updatedThisWeek;
	}
	
	public int getLastWeekUpdated() {
		return this.lastWeekUpdated;
	}
	
	public void setLastWeekUpdated(int lastWeekUpdated) {
		this.lastWeekUpdated = lastWeekUpdated;
	}

	public int getLastYearUpdated() {
		return this.lastYearUpdated;
	}
	
	public void setLastYearUpdated(int lastYearUpdated) {
		this.lastYearUpdated = lastYearUpdated;
	}

	public CourtDayTimes getThisWeekWeekdayHours() {
		return thisWeekWeekdayHours;
	}

	public void setThisWeekWeekdayHours(CourtDayTimes thisWeekWeekdayHours) {
		this.thisWeekWeekdayHours = thisWeekWeekdayHours;
	}

	public CourtDayTimes getThisSaturdayHours() {
		return thisSaturdayHours;
	}

	public void setThisSaturdayHours(CourtDayTimes thisSaturdayHours) {
		this.thisSaturdayHours = thisSaturdayHours;
	}

	public CourtDayTimes getThisSundayHours() {
		return thisSundayHours;
	}

	public void setThisSundayHours(CourtDayTimes thisSundayHours) {
		this.thisSundayHours = thisSundayHours;
	}

	public boolean openNow() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone("America/Chicago"));
		int currDay = calendar.get(Calendar.DAY_OF_WEEK);

		switch (currDay) {
		case Calendar.SATURDAY:
			return openNowHelper(calendar, this.thisSaturdayHours);
		case Calendar.SUNDAY:
			return openNowHelper(calendar, this.thisSundayHours);
		default:
			return openNowHelper(calendar, this.thisWeekWeekdayHours);
		}

	}

	private boolean openNowHelper(Calendar calendar, CourtDayTimes currDayTimes) {
		ArrayList<TimeFrame> timeFrames = currDayTimes.getCourtTimes();
		int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);

		for(TimeFrame tf : timeFrames) {
			if(currTimeInTimeFrame(hourOfDay, tf)) {
				return true;
			}
		}

		return false;
	}

	private boolean currTimeInTimeFrame(int hourOfDay, TimeFrame tf) {
		int open = tf.getOpen();
		int close = tf.getClose();

		return (open <= hourOfDay) && (hourOfDay < close);
	}

}
