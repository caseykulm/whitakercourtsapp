package com.mlukyesac.whitakercourts.models;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CourtDayTimes {
	
	private ArrayList<TimeFrame> courtTimes;
	private String rawCourtTimes;
	
	public CourtDayTimes(String rawCourtTimes) {
		
		this.rawCourtTimes = rawCourtTimes;
		courtTimes = new ArrayList<TimeFrame>();
		String[] timeFrames = rawCourtTimes.split(" ");
        courtTimes.add(new TimeFrame(timeFrames[0], timeFrames[2]));
	}
	
	public ArrayList<TimeFrame> getCourtTimes() {
		return courtTimes;
	}
	
	public String getRawCourtTimes() {
		return this.rawCourtTimes;
	}
	
	@Override
	public String toString() {
		String allTimes = "";
		for(TimeFrame timeFrameObj : courtTimes) {
			allTimes += timeFrameObj.toString() + "\n";
		}
		return allTimes;
	}
	
	public class TimeFrame {
		
		private int open, close;
		private Pattern morningPattern = Pattern.compile("(\\d+)(a)"); // will be capture group 1
		private Pattern eveningPattern = Pattern.compile("(\\d+)(p)"); // will be capture group 1
		
		private TimeFrame(String open, String close) {

			this.open = convertStrTime(open);
			this.close = convertStrTime(close);
		}
		
		private int convertStrTime(String timeStr) {
			if(timeStr.equals("Noon")) {
				return 12;
			}
			else {
				Matcher morningMatcher = this.morningPattern.matcher(timeStr);
				Matcher eveningMatcher = this.eveningPattern.matcher(timeStr);
				
				if(morningMatcher.matches()) {
					String hourStr = morningMatcher.group(1);
					int hourInt = Integer.parseInt(hourStr);
					String ampm = morningMatcher.group(2);
					return (ampm.equals("a")) ? hourInt : hourInt+12;
					
				}
				else if(eveningMatcher.matches()) {
					String hourStr = eveningMatcher.group(1);
					int hourInt = Integer.parseInt(hourStr);
					String ampm = eveningMatcher.group(2);
					return (ampm.equals("p")) ? hourInt : hourInt+12;
				}
				else {
					return -1;
				}
			}
			
		}

		public int getOpen() {
			return this.open;
		}
		
		public int getClose() {
			return this.close;
		}
		
		@Override
		public String toString() {
			return Integer.toString(this.open) + "-" + Integer.toString(this.close);
		}
	}
}
