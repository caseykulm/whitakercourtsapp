package com.mlukyesac.whitakercourts.controllers;

import java.io.IOException;
import java.lang.annotation.ElementType;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.mlukyesac.whitakercourts.listeners.WhitakerCourtsListener;
import com.mlukyesac.whitakercourts.models.CourtDayTimes;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;

public class FetchThisWeekTimes extends AsyncTask<URL, Void, Element> {

	private ProgressDialog dialog;

	private WhitakerCourtsListener wcListener;

	public FetchThisWeekTimes(Activity containerActivity, Fragment containerFragment) {
		dialog = new ProgressDialog(containerActivity);
		this.wcListener = (WhitakerCourtsListener) containerFragment;
	}

	protected void onPreExecute() {
		this.dialog.setMessage("Loading...");
		this.dialog.show();
	}

	protected Element doInBackground(URL... urls) {
		Document doc = null;

		try {
			doc = Jsoup.connect("http://www.utrecsports.org/hours").get();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if(doc == null) {
            return null;
        }
        else {
            Elements rows = doc.getElementsByClass("row");
            Element row = rows.get(8);
            Elements divs = row.getElementsByTag("div");
            Element div = divs.get(0);
            Elements tables = div.getElementsByTag("table");
            Element table = tables.get(1);
            Elements tbodys = table.getElementsByTag("tbody");
            Element tbody = tbodys.get(0);
            Elements trs = tbody.getElementsByTag("tr");
            Element trTag = trs.get(15);
			return trTag;
        }

	}

	protected void onPostExecute(Element trTag) {
		Elements tdTags = trTag.getElementsByTag("td");

		Element weekdayTdTag = tdTags.get(1);
		String weekdayRawString = weekdayTdTag.text();
		Element saturdayTdTag = tdTags.get(2);
		String saturdayRawString = saturdayTdTag.text();
		Element sundayTdTag = tdTags.get(3);
		String sundayRawString = sundayTdTag.text();

		CourtDayTimes thisWeek = new CourtDayTimes(weekdayRawString);
		CourtDayTimes thisSaturday = new CourtDayTimes(saturdayRawString);
		CourtDayTimes thisSunday = new CourtDayTimes(sundayRawString);

		if (dialog.isShowing()) {
			dialog.dismiss();
		}

		wcListener.thisWeekTimesChanged(thisWeek, thisSaturday, thisSunday);
	}

}